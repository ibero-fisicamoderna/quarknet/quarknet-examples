from ROOT import TFile, TCanvas, TH1F, THStack, TF1, TLegend, gROOT, gStyle
from ROOT import kTRUE, kFullCircle, kRed, kBlue


class PulsePattern(object):

    def __init__(self, *args):
        self.pattern = list(args)

    def __len__(self):
        return len(self.pattern)

    def __eq__(self, obj):
        if not isinstance(obj, PulsePattern):
            return False

        pattern = obj

        if len(self.pattern) != len(pattern):
            return False

        for i in range(len(self.pattern)):
            if self.get(i) != pattern.get(i):
                return False

        return True

    def get(self, i):
        return self.pattern[i]

class Pulse(object):

    def __init__(self, channel,
            startdate, stopdate,
            starttime, stoptime,
            starttime_nanos, stoptime_nanos,
            duration):
        self.channel = channel
        self.startdate = startdate
        self.stopdate = stopdate
        self.starttime = starttime
        self.stoptime = stoptime
        self.starttime_nanos = starttime_nanos
        self.stoptime_nanos = stoptime_nanos
        self.duration = duration

class PulseFamily(object):

    def __init__(self, num):
        self.num = num
        self.pulse_count = 0
        self.pulses = list()
        self.channel_pulse = [False] * 4

    def accept(self, pulse):
        if self.channel_pulse[pulse.channel]:
            return False

        # CHECK PULSE
        keep = True

        if self.pulse_count > 0:
            previous_pulse = self.last_pulse()

            dt = (pulse.startdate - previous_pulse.startdate) * 1e9
            dt += (pulse.starttime - previous_pulse.starttime) * 1e9
            dt += (pulse.starttime_nanos - previous_pulse.starttime_nanos)

            keep = (0 <= dt <= previous_pulse.duration)

        if keep:
            self.channel_pulse[pulse.channel] = True
            self.pulses.append(pulse)
            self.pulse_count += 1

        return keep

    def last_pulse(self):
        return self.pulses[self.pulse_count - 1]

    def get_channel_pulse(self, channel):
        for pulse in self.pulses:
            if pulse.channel == channel:
                return pulse

        return None

    def get_hit_pattern(self):
        hits = []

        for pulse in self.pulses:
            hits.append(pulse.channel)

        return PulsePattern(*hits)

class PulseFamilyBuilder(object):

    def __init__(self):
        self.num = 0
        self.family = None
        self.families = list()

    def process(self, pulse):
        if self.family is None:
            self.family = PulseFamily(self.num)
            self.family.accept(pulse)
            self.num += 1
        elif not self.family.accept(pulse):
            self.families.append(self.family)
            self.family = PulseFamily(self.num)
            self.family.accept(pulse)
            self.num += 1

    def get_families(self):
        ret_families = self.families[:]

        if self.family is not None:
            ret_families.append(self.family)

        return ret_families

###########################################################################
# MAIN ####################################################################
###########################################################################
gROOT.SetBatch(kTRUE)

# root_file = TFile.Open('data/EQUIP_9OCT2019_160613.root')
root_file = TFile.Open('data/EQUIP_26NOV2019_165149.root')

th1f_all = TH1F('gen_particle', 'Generation Particle', 11, -0.5, 10.5)

for event in root_file.ptree:
    # BUILD FAMILIES
    builder = PulseFamilyBuilder()

    nPulses = event.nPulses

    for iPulse in range(nPulses):
        channel = event.channel[iPulse]
        startdate = event.startdate[iPulse]
        stopdate = event.stopdate[iPulse]
        starttime = event.starttime[iPulse]
        stoptime = event.stoptime[iPulse]
        starttime_nanos = event.starttimeNanos[iPulse]
        stoptime_nanos = event.stoptimeNanos[iPulse]
        duration = event.duration[iPulse]

        pulse = Pulse(channel,
                startdate, stopdate,
                starttime, stoptime,
                starttime_nanos, stoptime_nanos,
                duration)

        builder.process(pulse)

    # CHECK FAMILIES
    for family in builder.get_families():
        th1f_all.Fill(family.num)

# PLOT
th1f_all.GetXaxis().SetTitle('Gen')
th1f_all.GetYaxis().SetTitle('Count')

gStyle.SetOptStat(0)

canvas = TCanvas('gen_particle', 'Generation Particle', 1920, 1080)
canvas.SetLogy(1)
canvas.cd()

th1f_all.GetPainter().PaintStat(111, 0)
canvas.Modified()

th1f_all.Draw()

canvas.SaveAs('gen_particle.png')
