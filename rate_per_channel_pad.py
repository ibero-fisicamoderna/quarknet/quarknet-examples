from ROOT import TFile, TCanvas, TH1F, THStack, TLegend, gROOT, gStyle, TPad
from ROOT import kYellow, kBlue, kGreen, kMagenta, kTRUE

class RateCalculator:

    def __init__(self, window):
        self.window = window

        self.count = 0

        self.open = 0
        self.startdate = -1
        self.starttime = -1
        self.starttime_nanos = -1

    def process(self, pulse):
        if not self.open:
            self.open = True

            self.count = 1
            self.startdate = pulse.startdate
            self.starttime = pulse.starttime
            self.starttime_nanos = pulse.starttime_nanos
        else:
            delta_t = pulse.startdate - self.startdate
            delta_t += pulse.starttime - self.starttime
            delta_t += (pulse.starttime_nanos - self.starttime_nanos) * 1e-9

            if delta_t <= self.window:
                self.count += 1
            else:
                temp = self.count

                self.count = 1
                self.startdate = pulse.startdate
                self.starttime = pulse.starttime
                self.starttime_nanos = pulse.starttime_nanos

                return temp

        return None

class Pulse(object):

    def __init__(self, channel,
            startdate, stopdate,
            starttime, stoptime,
            starttime_nanos, stoptime_nanos,
            duration):
        self.channel = channel
        self.startdate = startdate
        self.stopdate = stopdate
        self.starttime = starttime
        self.stoptime = stoptime
        self.starttime_nanos = starttime_nanos
        self.stoptime_nanos = stoptime_nanos
        self.duration = duration

###########################################################################
# MAIN ####################################################################
###########################################################################
gROOT.SetBatch(kTRUE)

gtc_duration = 1 # 1 Second

# root_file = TFile.Open('data/EQUIP_6SEP2019_102735.root')
# root_file = TFile.Open('data/EQUIP_18SEP2019_111649.root')
# root_file = TFile.Open('data/EQUIP_9OCT2019_160613.root')
root_file = TFile.Open('data/EQUIP_26NOV2019_165149.root')

th1f_0 = TH1F('channel0', 'Channel 0', 150, 0, 150)
th1f_1 = TH1F('channel1', 'Channel 1', 150, 0, 150)
th1f_2 = TH1F('channel2', 'Channel 2', 150, 0, 150)
th1f_3 = TH1F('channel3', 'Channel 3', 150, 0, 150)

ch0_rate = RateCalculator(1)
ch1_rate = RateCalculator(1)
ch2_rate = RateCalculator(1)
ch3_rate = RateCalculator(1)

for event in root_file.ptree:
    gtc = event.gtc
    nPulses = event.nPulses

    for iPulse in range(nPulses):
        channel = event.channel[iPulse]
        startdate = event.startdate[iPulse]
        stopdate = event.stopdate[iPulse]
        starttime = event.starttime[iPulse]
        stoptime = event.stoptime[iPulse]
        starttime_nanos = event.starttimeNanos[iPulse]
        stoptime_nanos = event.stoptimeNanos[iPulse]
        duration = event.duration[iPulse]

        pulse = Pulse(channel,
                startdate, stopdate,
                starttime, stoptime,
                starttime_nanos, stoptime_nanos,
                duration)

        if channel == 0:
            rate = ch0_rate.process(pulse)

            if rate is not None:
                th1f_0.Fill(rate)
        elif channel == 1:
            rate = ch1_rate.process(pulse)

            if rate is not None:
                th1f_1.Fill(rate)
        elif channel == 2:
            rate = ch2_rate.process(pulse)

            if rate is not None:
                th1f_2.Fill(rate)
        elif channel == 3:
            rate = ch3_rate.process(pulse)

            if rate is not None:
                th1f_3.Fill(rate)

th1f_0.SetFillColor(kYellow)
th1f_1.SetFillColor(kBlue)
th1f_2.SetFillColor(kGreen)
th1f_3.SetFillColor(kMagenta)

th1f_0.GetXaxis().SetTitle('Rate [s^{-1}]')
th1f_0.GetYaxis().SetTitle('Count')
th1f_1.GetXaxis().SetTitle('Rate [s^{-1}]')
th1f_1.GetYaxis().SetTitle('Count')
th1f_2.GetXaxis().SetTitle('Rate [s^{-1}]')
th1f_2.GetYaxis().SetTitle('Count')
th1f_3.GetXaxis().SetTitle('Rate [s^{-1}]')
th1f_3.GetYaxis().SetTitle('Count')

canvas = TCanvas('rate_per_channel_pad', 'Rate per Channel', 1920, 1080)

padCh0 = TPad("Ch0","",0.005,0.505,0.495,0.995)
padCh0.cd()
th1f_0.Draw()
th1f_0.Fit('gaus')
th1f_0.Draw('SAME')

padCh1 = TPad("Ch1","",0.505,0.505,0.995,0.995)
padCh1.cd()
th1f_1.Draw()
th1f_1.Fit('gaus')
th1f_1.Draw('SAME')

padCh2 = TPad("Ch2","",0.005,0.005,0.495,0.495)
padCh2.cd()
th1f_2.Draw()
th1f_2.Fit('gaus')
th1f_2.Draw('SAME')

padCh3 = TPad("Ch3","",0.505,0.005,0.995,0.495)
padCh3.cd()
th1f_3.Draw()
th1f_3.Fit('gaus')
th1f_3.Draw('SAME')

canvas.cd()
padCh0.Draw()
padCh1.Draw()
padCh2.Draw()
padCh3.Draw()

canvas.SaveAs('rate_per_channel_pad.png')
