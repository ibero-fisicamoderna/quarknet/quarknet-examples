from ROOT import TFile

root_file = TFile.Open('data/data.root')

for event in root_file.ptree:
    gtc = event.gtc
    nPulses = event.nPulses

    for iPulse in range(nPulses):
        channel = event.channel[iPulse]
        startdate = event.startdate[iPulse]
        stopdate = event.stopdate[iPulse]
        starttime = event.starttime[iPulse]
        stoptime = event.stoptime[iPulse]
        starttime_nanos = event.starttimeNanos[iPulse]
        stoptime_nanos = event.stoptimeNanos[iPulse]
        duration = event.duration[iPulse]

        print(gtc, channel,
                startdate, stopdate,
                starttime, stoptime,
                starttime_nanos, stoptime_nanos,
                duration)
