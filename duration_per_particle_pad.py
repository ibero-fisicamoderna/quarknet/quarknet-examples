from ROOT import TFile, TCanvas, TH1F, THStack, TLegend, gROOT, gStyle, TPad
from ROOT import kRed, kBlue, kTRUE


class PulsePattern(object):

    def __init__(self, *args):
        self.pattern = list(args)

    def __len__(self):
        return len(self.pattern)

    def __eq__(self, obj):
        if not isinstance(obj, PulsePattern):
            return False

        pattern = obj

        if len(self.pattern) != len(pattern):
            return False

        for i in range(len(self.pattern)):
            if self.get(i) != pattern.get(i):
                return False

        return True

    def get(self, i):
        return self.pattern[i]

class Pulse(object):

    def __init__(self, channel,
            startdate, stopdate,
            starttime, stoptime,
            starttime_nanos, stoptime_nanos,
            duration):
        self.channel = channel
        self.startdate = startdate
        self.stopdate = stopdate
        self.starttime = starttime
        self.stoptime = stoptime
        self.starttime_nanos = starttime_nanos
        self.stoptime_nanos = stoptime_nanos
        self.duration = duration

class PulseFamily(object):

    def __init__(self, num):
        self.num = num
        self.pulse_count = 0
        self.pulses = list()
        self.channel_pulse = [False] * 4

    def accept(self, pulse):
        if self.channel_pulse[pulse.channel]:
            return False

        # CHECK PULSE
        keep = True

        if self.pulse_count > 0:
            previous_pulse = self.last_pulse()

            dt = (pulse.startdate - previous_pulse.startdate) * 1e9
            dt += (pulse.starttime - previous_pulse.starttime) * 1e9
            dt += (pulse.starttime_nanos - previous_pulse.starttime_nanos)

            keep = (0 <= dt <= previous_pulse.duration)

        if keep:
            self.channel_pulse[pulse.channel] = True
            self.pulses.append(pulse)
            self.pulse_count += 1

        return keep

    def last_pulse(self):
        return self.pulses[self.pulse_count - 1]

    def get_channel_pulse(self, channel):
        for pulse in self.pulses:
            if pulse.channel == channel:
                return pulse

        return None

    def get_hit_pattern(self):
        hits = []

        for pulse in self.pulses:
            hits.append(pulse.channel)

        return PulsePattern(*hits)

class PulseFamilyBuilder(object):

    def __init__(self):
        self.num = 0
        self.family = None
        self.families = list()

    def process(self, pulse):
        if self.family is None:
            self.family = PulseFamily(self.num)
            self.family.accept(pulse)
            self.num += 1
        elif not self.family.accept(pulse):
            self.families.append(self.family)
            self.family = PulseFamily(self.num)
            self.family.accept(pulse)
            self.num += 1

    def get_families(self):
        ret_families = self.families[:]

        if self.family is not None:
            ret_families.append(self.family)

        return ret_families

###########################################################################
# MAIN ####################################################################
###########################################################################
gROOT.SetBatch(kTRUE)

# root_file = TFile.Open('data/EQUIP_9OCT2019_160613.root')
root_file = TFile.Open('data/EQUIP_26NOV2019_165149.root')

init_gen_number = 0

th1f_mu = TH1F('muon', 'Muon', 80, 0, 100)
th1f_e = TH1F('electron', 'Electron', 80, 0, 100)

# HIT PATTERNS
# NOTE: THESE PATTERNS ARE STRICT, IT EXPECTS THE MUON TO HIT AT LEAST ONE LAYER
# BEFORE DECAYING
# hit_patterns = [
#         PulsePattern(1, 0),
#         PulsePattern(0, 1),
#         PulsePattern(0, 1, 2),
#         PulsePattern(1, 2),
#         PulsePattern(2, 1),
#         PulsePattern(2, 1, 0)]

# NOTE: THESE PATTERNS ARE LOOSE, THEY EXPECT THE MUON TO HIT ANY LAYER
# AND DECAY
# hit_patterns = [
#         PulsePattern(0),
#         PulsePattern(1),
#         PulsePattern(2)]

# NOTE: ALL VALID PATTERNS
hit_patterns = [
        PulsePattern(0),
        PulsePattern(1),
        PulsePattern(2),
        PulsePattern(1, 0),
        PulsePattern(0, 1),
        PulsePattern(0, 1, 2),
        PulsePattern(1, 2),
        PulsePattern(2, 1),
        PulsePattern(2, 1, 0)]

for event in root_file.ptree:
    # BUILD FAMILIES
    builder = PulseFamilyBuilder()

    nPulses = event.nPulses

    for iPulse in range(nPulses):
        channel = event.channel[iPulse]
        startdate = event.startdate[iPulse]
        stopdate = event.stopdate[iPulse]
        starttime = event.starttime[iPulse]
        stoptime = event.stoptime[iPulse]
        starttime_nanos = event.starttimeNanos[iPulse]
        stoptime_nanos = event.stoptimeNanos[iPulse]
        duration = event.duration[iPulse]

        pulse = Pulse(channel,
                startdate, stopdate,
                starttime, stoptime,
                starttime_nanos, stoptime_nanos,
                duration)

        builder.process(pulse)

    # CHECK FAMILIES
    first_family = None
    second_family = None

    for family in builder.get_families():
        hit_pattern = family.get_hit_pattern()

        if first_family is None:
            if family.num == init_gen_number and hit_pattern in hit_patterns:
                first_family = family
        else:
            decay_channel = first_family.last_pulse().channel

            if hit_pattern.pattern[0] == decay_channel:
                second_family = family
            else:
                break

        if first_family is not None and second_family is not None:
            decay_channel = second_family.get_hit_pattern().pattern[0]

            primary = first_family.get_channel_pulse(decay_channel)
            secondary = second_family.get_channel_pulse(decay_channel)

            tol = (secondary.startdate - primary.startdate) * 1e6
            tol += (secondary.starttime - primary.starttime) * 1e6
            tol += (secondary.starttime_nanos - primary.starttime_nanos) * 1e-3

            # NOTE: TOL IS IN MICROS AND DURATION IS IN NANOS
            if 0.1 <= tol <= 10:
                th1f_mu.Fill(primary.duration)
                th1f_e.Fill(secondary.duration)

                first_family = None
                second_family = None
            elif tol < 0.1:
                second_family = None
            else:
                first_family = None
                second_family = None

# PLOT
mu_max = th1f_mu.GetMaximum()
e_max = th1f_mu.GetMaximum()
global_max = max(mu_max, e_max) * 1.1

th1f_mu.SetFillColor(kRed)
th1f_e.SetFillColor(kBlue)

th1f_mu.GetXaxis().SetTitle('Duration [ns]')
th1f_mu.GetYaxis().SetTitle('Count')
th1f_mu.SetMaximum(global_max)
th1f_e.GetXaxis().SetTitle('Duration [ns]')
th1f_e.GetYaxis().SetTitle('Count')
th1f_e.SetMaximum(global_max)

canvas = TCanvas('duration_per_particle_pad', 'Duration per Particle', 1920, 1080)

pad_mu = TPad("Muon","",0.005,0.005,0.495,0.995)
pad_mu.cd()
th1f_mu.Draw()
th1f_mu.Draw('SAME')

pad_e = TPad("Electron","",0.505,0.005,0.995,0.995)
pad_e.cd()
th1f_e.Draw()
th1f_e.Draw('SAME')

canvas.cd()
pad_mu.Draw()
pad_e.Draw()

canvas.SaveAs('duration_per_particle_pad_g%d.png' % init_gen_number)
