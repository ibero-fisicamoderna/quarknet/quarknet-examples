from ROOT import TFile, TCanvas, TH1F, THStack, TLegend, gROOT, gStyle, TPad
from ROOT import kRed, kBlue, kGreen, kMagenta, kTRUE

###########################################################################
# MAIN ####################################################################
###########################################################################
gROOT.SetBatch(kTRUE)

# root_file = TFile.Open('data/EQUIP_6SEP2019_102735.root')
# root_file = TFile.Open('data/EQUIP_18SEP2019_111649.root')
# root_file = TFile.Open('data/EQUIP_9OCT2019_160613.root')
root_file = TFile.Open('data/EQUIP_26NOV2019_165149.root')

th1f_0 = TH1F('tof0', 'Time of Flight CH0 - CH1', 80, -50, 50)
th1f_1 = TH1F('tof1', 'Time of Flight CH0 - CH2', 80, -50, 50)
th1f_2 = TH1F('tof2', 'Time of Flight CH0 - CH3', 80, -50, 50)
th1f_3 = TH1F('tof3', 'Time of Flight CH1 - CH2', 80, -50, 50)
th1f_4 = TH1F('tof4', 'Time of Flight CH1 - CH3', 80, -50, 50)
th1f_5 = TH1F('tof5', 'Time of Flight CH2 - CH3', 80, -50, 50)

def fill_plot(thf, starttimes0, starttimes1):
    if len(starttimes0) > 0 and len(starttimes1) > 0:
        first_st0 = starttimes0[0]
        first_st1 = starttimes1[0]

        if first_st0[0] == first_st1[0]:
            t_flight = (first_st0[0] - first_st1[0]) * 1e9
            t_flight += (first_st0[1] - first_st1[1])
            thf.Fill(t_flight)

for event in root_file.ptree:
    gtc = event.gtc
    nPulses = event.nPulses
    starttime = event.starttime
    starttimeNanos = event.starttimeNanos

    starttimes0 = list()
    starttimes1 = list()
    starttimes2 = list()
    starttimes3 = list()

    for iPulse in range(nPulses):
        channel = event.channel[iPulse]
        pStarttime = starttime[iPulse]
        pStarttimeNanos = starttimeNanos[iPulse]

        if channel == 0:
            starttimes0.append((pStarttime, pStarttimeNanos))

        elif channel == 1:
            starttimes1.append((pStarttime, pStarttimeNanos))

        elif channel == 2:
            starttimes2.append((pStarttime, pStarttimeNanos))

        elif channel == 3:
            starttimes3.append((pStarttime, pStarttimeNanos))

    fill_plot(th1f_0, starttimes0, starttimes1)
    fill_plot(th1f_1, starttimes0, starttimes2)
    fill_plot(th1f_2, starttimes0, starttimes3)
    fill_plot(th1f_3, starttimes1, starttimes2)
    fill_plot(th1f_4, starttimes1, starttimes3)
    fill_plot(th1f_5, starttimes2, starttimes3)

th1f_0.SetFillColor(kBlue)
th1f_1.SetFillColor(kBlue)
th1f_2.SetFillColor(kBlue)
th1f_3.SetFillColor(kBlue)
th1f_4.SetFillColor(kBlue)
th1f_5.SetFillColor(kBlue)

th1f_0.GetXaxis().SetTitle('ToF [ns]')
th1f_0.GetYaxis().SetTitle('Count')
th1f_1.GetXaxis().SetTitle('ToF [ns]')
th1f_1.GetYaxis().SetTitle('Count')
th1f_2.GetXaxis().SetTitle('ToF [ns]')
th1f_2.GetYaxis().SetTitle('Count')
th1f_3.GetXaxis().SetTitle('ToF [ns]')
th1f_3.GetYaxis().SetTitle('Count')
th1f_4.GetXaxis().SetTitle('ToF [ns]')
th1f_4.GetYaxis().SetTitle('Count')
th1f_5.GetXaxis().SetTitle('ToF [ns]')
th1f_5.GetYaxis().SetTitle('Count')

canvas = TCanvas("tof", "Time of Flight", 1920, 1080)
canvas.cd()

pad0 = TPad("Ch0 - Ch1","",0.005,0.671,0.495,0.995)
pad0.cd()
th1f_0.Draw()
th1f_0.Draw('SAME')

pad1 = TPad("Ch0 - Ch2","",0.505,0.671,0.995,0.995)
pad1.cd()
th1f_1.Draw()
th1f_1.Draw('SAME')

pad2 = TPad("Ch0 - Ch3","",0.005,0.338,0.495,0.661)
pad2.cd()
th1f_2.Draw()
th1f_2.Draw('SAME')

pad3 = TPad("Ch1 - Ch2","",0.505,0.338,0.995,0.661)
pad3.cd()
th1f_3.Draw()
th1f_3.Draw('SAME')

pad4 = TPad("Ch1 - Ch3","",0.005,0.005,0.495,0.328)
pad4.cd()
th1f_4.Draw()
th1f_4.Draw('SAME')

pad5 = TPad("Ch2 - Ch3","",0.505,0.005,0.995,0.328)
pad5.cd()
th1f_5.Draw()
th1f_5.Draw('SAME')

canvas.cd()
pad0.Draw()
pad1.Draw()
pad2.Draw()
pad3.Draw()
pad4.Draw()
pad5.Draw()

canvas.SaveAs('tof.png')
