from ROOT import TFile, TCanvas, TH1F, THStack, TLegend, gROOT, gStyle
from ROOT import kYellow, kBlue, kGreen, kMagenta, kTRUE

gROOT.SetBatch(kTRUE)

# root_file = TFile.Open('data/EQUIP_6SEP2019_102735.root')
# root_file = TFile.Open('data/EQUIP_18SEP2019_111649.root')
# root_file = TFile.Open('data/EQUIP_9OCT2019_160613.root')
root_file = TFile.Open('data/EQUIP_26NOV2019_165149.root')

thstack = THStack('pulse_duration_per_channel', 'Pulse Duration per Channel')
th1f_all = TH1F('pulse_duration_per_channel', 'Pulse Duration per Channel', 80, 0, 100)
th1f_0 = TH1F('channel0', 'Channel 0', 80, 0, 100)
th1f_1 = TH1F('channel1', 'Channel 1', 80, 0, 100)
th1f_2 = TH1F('channel2', 'Channel 2', 80, 0, 100)
th1f_3 = TH1F('channel3', 'Channel 3', 80, 0, 100)

x = .754
y = .800
legend = TLegend(x, y, x + 0.225, y - .225)

for event in root_file.ptree:
    nPulses = event.nPulses
    duration = event.duration

    for iPulse in range(nPulses):
        channel = event.channel[iPulse]
        pDuration = duration[iPulse]

        if pDuration < 0:
            continue

        if channel == 0 :
            th1f_0.Fill(pDuration)
            th1f_all.Fill(pDuration)
        elif channel == 1 :
            th1f_1.Fill(pDuration)
            th1f_all.Fill(pDuration)
        elif channel == 2 :
            th1f_2.Fill(pDuration)
            th1f_all.Fill(pDuration)
        elif channel == 3 :
            th1f_3.Fill(pDuration)
            th1f_all.Fill(pDuration)

th1f_0.SetFillColor(kYellow)
th1f_1.SetFillColor(kBlue)
th1f_2.SetFillColor(kGreen)
th1f_3.SetFillColor(kMagenta)

legend.AddEntry(th1f_0, 'Channel 0 - \Delta t=%.2f' % th1f_0.GetMean(), 'F')
legend.AddEntry(th1f_1, 'Channel 1 - \Delta t=%.2f' % th1f_1.GetMean(), 'F')
legend.AddEntry(th1f_2, 'Channel 2 - \Delta t=%.2f' % th1f_2.GetMean(), 'F')
legend.AddEntry(th1f_3, 'Channel 3 - \Delta t=%.2f' % th1f_3.GetMean(), 'F')

thstack.Add(th1f_0)
thstack.Add(th1f_1)
thstack.Add(th1f_2)
thstack.Add(th1f_3)

th1f_all.GetXaxis().SetTitle('Duration [ns]')
th1f_all.GetYaxis().SetTitle('Count')

gStyle.SetOptStat(0)

canvas = TCanvas('duration_per_channel', 'Duration per Channel', 1920, 1080)
canvas.cd()

th1f_all.GetPainter().PaintStat(111, 0)
canvas.Modified()

th1f_all.Draw()
thstack.Draw('SAME')
thstack.Draw('SAME AXIS')
th1f_all.Fit('gaus')
th1f_all.Draw('SAME')
legend.Draw()

canvas.SaveAs('duration_per_channel.png')
