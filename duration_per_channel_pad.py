from ROOT import TFile, TCanvas, TH1F, THStack, TLegend, gROOT, gStyle, TPad
from ROOT import kYellow, kBlue, kGreen, kMagenta, kTRUE

gROOT.SetBatch(kTRUE)

# root_file = TFile.Open('data/EQUIP_6SEP2019_102735.root')
# root_file = TFile.Open('data/EQUIP_18SEP2019_111649.root')
# root_file = TFile.Open('data/EQUIP_9OCT2019_160613.root')
root_file = TFile.Open('data/EQUIP_26NOV2019_165149.root')

th1f_0 = TH1F('channel0', 'Channel 0', 80, 0, 100)
th1f_1 = TH1F('channel1', 'Channel 1', 80, 0, 100)
th1f_2 = TH1F('channel2', 'Channel 2', 80, 0, 100)
th1f_3 = TH1F('channel3', 'Channel 3', 80, 0, 100)

for event in root_file.ptree:
    gtc = event.gtc
    nPulses = event.nPulses
    duration = event.duration

    for iPulse in range(nPulses):
        channel = event.channel[iPulse]
        pDuration = duration[iPulse]

        if pDuration < 0:
            continue

        if channel == 0 :
            th1f_0.Fill(pDuration)
        elif channel == 1 :
            th1f_1.Fill(pDuration)
        elif channel == 2 :
            th1f_2.Fill(pDuration)
        elif channel == 3 :
            th1f_3.Fill(pDuration)

th1f_0.SetFillColor(kYellow)
th1f_1.SetFillColor(kBlue)
th1f_2.SetFillColor(kGreen)
th1f_3.SetFillColor(kMagenta)

th1f_0.GetXaxis().SetTitle('Duration [ns]')
th1f_0.GetYaxis().SetTitle('Count')
th1f_1.GetXaxis().SetTitle('Duration [ns]')
th1f_1.GetYaxis().SetTitle('Count')
th1f_2.GetXaxis().SetTitle('Duration [ns]')
th1f_2.GetYaxis().SetTitle('Count')
th1f_3.GetXaxis().SetTitle('Duration [ns]')
th1f_3.GetYaxis().SetTitle('Count')

canvas = TCanvas('duration_per_channel_pad', 'Duration per Channel', 1920, 1080)

padCh0 = TPad("Ch0","",0.005,0.505,0.495,0.995)
padCh0.cd()
th1f_0.Draw()
th1f_0.Fit('gaus')
th1f_0.Draw('SAME')

padCh1 = TPad("Ch1","",0.505,0.505,0.995,0.995)
padCh1.cd()
th1f_1.Draw()
th1f_1.Fit('gaus')
th1f_1.Draw('SAME')

padCh2 = TPad("Ch2","",0.005,0.005,0.495,0.495)
padCh2.cd()
th1f_2.Draw()
th1f_2.Fit('gaus')
th1f_2.Draw('SAME')

padCh3 = TPad("Ch3","",0.505,0.005,0.995,0.495)
padCh3.cd()
th1f_3.Draw()
th1f_3.Fit('gaus')
th1f_3.Draw('SAME')

canvas.cd()
padCh0.Draw()
padCh1.Draw()
padCh2.Draw()
padCh3.Draw()

canvas.SaveAs('duration_per_channel_pad.png')
