from ROOT import TFile, TCanvas, TH1F, THStack, TLegend, gROOT, gStyle
from ROOT import kYellow, kBlue, kGreen, kMagenta, kTRUE

class RateCalculator:

    def __init__(self, window):
        self.window = window

        self.count = 0

        self.open = 0
        self.startdate = -1
        self.starttime = -1
        self.starttime_nanos = -1

    def process(self, pulse):
        if not self.open:
            self.open = True

            self.count = 1
            self.startdate = pulse.startdate
            self.starttime = pulse.starttime
            self.starttime_nanos = pulse.starttime_nanos
        else:
            delta_t = pulse.startdate - self.startdate
            delta_t += pulse.starttime - self.starttime
            delta_t += (pulse.starttime_nanos - self.starttime_nanos) * 1e-9

            # NOTE: DELTA T IS IN SECONDS
            if delta_t <= self.window:
                self.count += 1
            else:
                temp = self.count

                self.count = 1
                self.startdate = pulse.startdate
                self.starttime = pulse.starttime
                self.starttime_nanos = pulse.starttime_nanos

                return temp

        return None

class Pulse(object):

    def __init__(self, channel,
            startdate, stopdate,
            starttime, stoptime,
            starttime_nanos, stoptime_nanos,
            duration):
        self.channel = channel
        self.startdate = startdate
        self.stopdate = stopdate
        self.starttime = starttime
        self.stoptime = stoptime
        self.starttime_nanos = starttime_nanos
        self.stoptime_nanos = stoptime_nanos
        self.duration = duration

###########################################################################
# MAIN ####################################################################
###########################################################################
gROOT.SetBatch(kTRUE)

gtc_duration = 1 # 1 Second

# root_file = TFile.Open('data/EQUIP_6SEP2019_102735.root')
# root_file = TFile.Open('data/EQUIP_18SEP2019_111649.root')
# root_file = TFile.Open('data/EQUIP_9OCT2019_160613.root')
root_file = TFile.Open('data/EQUIP_26NOV2019_165149.root')

thstack = THStack('pulse_rate_per_channel', 'Pulse Rate per Channel')
th1f_all = TH1F('pulse_rate_per_channel', 'Pulse Rate per Channel', 150, 0, 150)
th1f_0 = TH1F('channel0', 'Channel 0', 150, 0, 150)
th1f_1 = TH1F('channel1', 'Channel 1', 150, 0, 150)
th1f_2 = TH1F('channel2', 'Channel 2', 150, 0, 150)
th1f_3 = TH1F('channel3', 'Channel 3', 150, 0, 150)

x = .754
y = .800
legend = TLegend(x, y, x + 0.225, y - .225)

rates0 = []
rates1 = []
rates2 = []
rates3 = []

ch0_rate = RateCalculator(1)
ch1_rate = RateCalculator(1)
ch2_rate = RateCalculator(1)
ch3_rate = RateCalculator(1)

for event in root_file.ptree:
    gtc = event.gtc
    nPulses = event.nPulses

    for iPulse in range(nPulses):
        channel = event.channel[iPulse]
        startdate = event.startdate[iPulse]
        stopdate = event.stopdate[iPulse]
        starttime = event.starttime[iPulse]
        stoptime = event.stoptime[iPulse]
        starttime_nanos = event.starttimeNanos[iPulse]
        stoptime_nanos = event.stoptimeNanos[iPulse]
        duration = event.duration[iPulse]

        pulse = Pulse(channel,
                startdate, stopdate,
                starttime, stoptime,
                starttime_nanos, stoptime_nanos,
                duration)

        if channel == 0:
            rate = ch0_rate.process(pulse)

            if rate is not None:
                rates0.append(rate)
                th1f_0.Fill(rate)
                th1f_all.Fill(rate)
        elif channel == 1:
            rate = ch1_rate.process(pulse)

            if rate is not None:
                rates1.append(rate)
                th1f_1.Fill(rate)
                th1f_all.Fill(rate)
        elif channel == 2:
            rate = ch2_rate.process(pulse)

            if rate is not None:
                rates2.append(rate)
                th1f_2.Fill(rate)
                th1f_all.Fill(rate)
        elif channel == 3:
            rate = ch3_rate.process(pulse)

            if rate is not None:
                rates3.append(rate)
                th1f_3.Fill(rate)
                th1f_all.Fill(rate)

th1f_0.SetFillColor(kYellow)
th1f_1.SetFillColor(kBlue)
th1f_2.SetFillColor(kGreen)
th1f_3.SetFillColor(kMagenta)

rate0 = 0
rate1 = 0
rate2 = 0
rate3 = 0

if len(rates0) > 0:
    rate0 = sum(rates0) / len(rates0)
if len(rates1) > 0:
    rate1 = sum(rates1) / len(rates1)
if len(rates2) > 0:
    rate2 = sum(rates2) / len(rates2)
if len(rates3) > 0:
    rate3 = sum(rates3) / len(rates3)

legend.AddEntry(th1f_0, 'Channel 0 - R=%.2f' % rate0, 'F')
legend.AddEntry(th1f_1, 'Channel 1 - R=%.2f' % rate1, 'F')
legend.AddEntry(th1f_2, 'Channel 2 - R=%.2f' % rate2, 'F')
legend.AddEntry(th1f_3, 'Channel 3 - R=%.2f' % rate3, 'F')

thstack.Add(th1f_0)
thstack.Add(th1f_1)
thstack.Add(th1f_2)
thstack.Add(th1f_3)

th1f_all.GetXaxis().SetTitle('Rate [s^{-1}]')
th1f_all.GetYaxis().SetTitle('Count')

gStyle.SetOptStat(0)

canvas = TCanvas('rate_per_channel', 'Rate per Channel', 1920, 1080)
canvas.cd()

th1f_all.GetPainter().PaintStat(111, 0)
canvas.Modified()

th1f_all.Draw()
thstack.Draw('SAME')
thstack.Draw('SAME AXIS')
th1f_all.Fit('gaus')
th1f_all.Draw('SAME')
legend.Draw()
canvas.SaveAs('rate_per_channel.png')
